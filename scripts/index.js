var modalWindow = document.querySelector('#modal-window');
var users = getUsers();

function getUsers() {
    var xhr = new XMLHttpRequest();

    xhr.open('GET', 'https://api.randomuser.me/1.0/?results=50&nat=gb,us&inc=gender,name,location,email,phone,picture', false);

    xhr.send();

    if (xhr.status != 200) {
        alert('loh');
    } else {
        return JSON.parse(xhr.response).results;
    }
}

function render() {
    var source = document.querySelector("#users-template").innerHTML;
    var template = Handlebars.compile(source);
    var context = users;
    var html = template(context);
    document.querySelector('#users').innerHTML = html;
}


function openModal(event) {
    modalWindow.style.display = 'block';
    var userInfo = users.filter(function(user) {
        return user.email === event.target.id;
    })[0];
    var source = document.querySelector("#user-template").innerHTML;
    var template = Handlebars.compile(source);
    var context = userInfo;
    var html = template(context);
    document.querySelector('#user').innerHTML = html;
}

function closeModal() {
    modalWindow.style.display = 'none';
}

function sort(event) {
    if (event.target.value === 'Alphabetical order') {
        users = users.sort(function(firstItem, secondItem) {
            if (firstItem.name.first > secondItem.name.first) return + 1;
            if (firstItem.name.first < secondItem.name.first) return - 1;
            return 0;
        });
    } else {
        users = users.sort(function(firstItem, secondItem) {
            if (firstItem.name.first > secondItem.name.first) return - 1;
            if (firstItem.name.first < secondItem.name.first) return + 1;
            return 0;
        });
    }
    render();
}

render();
